# Ripley Technical Challenge - Angular

Proyecto del lado frontend para el Desafío Técnico de Ripley, generado con [Angular CLI](https://github.com/angular/angular-cli) version 11.0.3.

## Development server

Para echar a correr el proyecto, utilizar el comando `ng serve` en la raíz del proyecto.
En tu navegador, dirígete a `http://localhost:4200/`.
