import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { LoginService }  from "./login/login.service";
import { BalanceService }  from "./balance/balance.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  title = 'rtc-angular';
  login: boolean = false;
  id: String = '';
  name: String = '';
  balance: String = '';

  showCollapse: boolean = false;

  constructor(private router: Router, private httpClient: HttpClient, private loginService: LoginService, private balanceService: BalanceService) { }

  ngOnInit() {
    this.loginService.loginCurrent.subscribe(current => {
      this.login = current;
    });
    this.balanceService.balanceCurrent.subscribe(current => {
      this.balance = current;
    });

    this.id = localStorage.getItem('userId')!;
    this.name = localStorage.getItem('userName')!;
    this.balance = localStorage.getItem('userBalance') != 'undefined' && localStorage.getItem('userBalance') != undefined ? localStorage.getItem('userBalance')! : '0';

    if(this.id != '') {
      let id = { id: this.id };
      this.httpClient.post<any[]>(this.appBaseUrl + '/balance', id).subscribe((res: any) => {
        this.balance = res.data.balance;
        localStorage.setItem('userBalance', res.data.balance);
        this.balanceService.balanceChange(res.data.balance);
      });
    }
  }

  logOut() {
    localStorage.clear();
    this.loginService.loginChange(false);
    this.router.navigate(['/login']);
  }
}
