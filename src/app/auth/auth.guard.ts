import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { LoginService }  from "../login/login.service";

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private router: Router, private loginService: LoginService){}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.loginService.loginCurrent.subscribe(current => {
      if(!current) { this.router.navigate(["/login"]); }
      return current;
    });
    if(localStorage.getItem('userId') != undefined) {
      return true;
    } else {
      return false;
    }
  }
}
