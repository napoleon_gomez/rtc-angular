import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {

  localBalance = localStorage.getItem('userBalance');
  actualBalance = this.localBalance != '' && this.localBalance != undefined && this.localBalance != 'undefined' ? this.localBalance : '0';

  private balanceSource = new BehaviorSubject<String>(this.actualBalance);
  balanceCurrent = this.balanceSource.asObservable();

  constructor() { }

  balanceChange(res: String) {
    this.balanceSource.next(res);
  }
}
