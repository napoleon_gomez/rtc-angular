import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validator, Validators, ReactiveFormsModule } from "@angular/forms";

import { BalanceService }  from "../balance/balance.service";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {
  //appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  loading: boolean = false;
  message: string = '';
  status: boolean = false;

  depositForm:     FormGroup;
  depositAmount:   FormControl;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private balanceService: BalanceService) {
    this.depositForm = formBuilder.group({
      depositAmount: [this.depositAmount, [Validators.required, Validators.min(1), Validators.max(999999999)]]
    });
  }

  ngOnInit(): void {
  }

  depositAmountToMyAccount(value: any = {}) {
    if(this.depositForm.valid) {
      let form = {
        id: localStorage.getItem('userId'),
        amount: value.depositAmount
      }
      this.loading = true;
      this.httpClient.post<any[]>(this.appBaseUrl + '/deposit', form).subscribe((res: any) => {
        this.loading = false;
        this.status = res.status;
        this.message = res.message;
        if(res.status) {
          localStorage.setItem('userBalance', res.data.balance);
          this.balanceService.balanceChange(res.data.balance);
          this.depositForm.get('depositAmount')!.setValue(0);
        }
        setTimeout(() => {
          this.message = '';
          this.status = false;
        }, 5000, this);
      });
    }
  }
}
