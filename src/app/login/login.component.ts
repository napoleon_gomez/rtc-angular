import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validator, Validators, ReactiveFormsModule } from "@angular/forms";
import { Router } from '@angular/router';

import { LoginService }  from "../login/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  loading: boolean = false;
  message: string = '';

  loginForm:     FormGroup;
  loginEmail:    FormControl;
  loginPassword: FormControl;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router, private loginService: LoginService) {
    this.loginForm = formBuilder.group({
      loginEmail:    [this.loginEmail, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      loginPassword: [this.loginPassword, [Validators.required, Validators.minLength(8)]]
    });
  }

  ngOnInit(): void {
    localStorage.clear();
  }

  loginUser(value: any = {}) {
    if(this.loginForm.valid) {
      let form = {
        email: value.loginEmail,
        password: value.loginPassword
      }
      this.loading = true;
      this.httpClient.post<any[]>(this.appBaseUrl + '/login', form).subscribe((res: any) => {
        this.loading = false;
        if(res.status) {
          localStorage.setItem('userId', res.data.id);
          localStorage.setItem('userName', res.data.name);
          localStorage.setItem('userBalance', res.data.balance);
          this.loginService.loginChange(true);
          this.router.navigate(['/transactions']).then(()=>{
            let url = window.location.href;
            window.location.replace(url);
          });
        } else {
          this.message = res.message;
        }
      });
    }
  }
}
