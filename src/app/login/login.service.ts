import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userExists = localStorage.getItem('userId') != '' && localStorage.getItem('userId') != undefined ? true : false;

  private loginSource = new BehaviorSubject<boolean>(this.userExists);
  loginCurrent = this.loginSource.asObservable();

  constructor() { }

  loginChange(res: boolean) {
    this.loginSource.next(res);
  }
}
