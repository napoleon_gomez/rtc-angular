import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validator, Validators, ReactiveFormsModule } from "@angular/forms";
import { Router } from '@angular/router';

import { LoginService }  from "../login/login.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  //appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  loading: boolean = false;
  message: string = '';

  registerForm:     FormGroup;
  registerName:     FormControl;
  registerRut:      FormControl;
  registerEmail:    FormControl;
  registerPassword: FormControl;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private router: Router, private loginService: LoginService) {
    this.registerForm = formBuilder.group({
      registerName:     [this.registerName, [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*')]],
      registerRut:      [this.registerRut, [Validators.required, Validators.minLength(9), Validators.pattern('[0-9]{7,8}-[0-9Kk]{1}')]],
      registerEmail:    [this.registerEmail, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      registerPassword: [this.registerPassword, [Validators.required, Validators.minLength(8)]]
    });
  }

  ngOnInit(): void {
    localStorage.clear();
  }

  registerUser(value: any = {}) {
    if(this.registerForm.valid) {
      let form = {
        name: value.registerName,
        rut: value.registerRut,
        email: value.registerEmail,
        password: value.registerPassword
      }
      this.loading = true;
      this.httpClient.post<any[]>(this.appBaseUrl + '/register', form).subscribe((res: any) => {
        this.loading = false;
        if(res.status) {
          localStorage.setItem('userId', res.data.id);
          localStorage.setItem('userName', res.data.name);
          localStorage.setItem('userBalance', res.data.balance);
          this.loginService.loginChange(true);
          this.router.navigate(['/transactions']).then(()=>{window.location.reload();});
        } else {
          this.message = res.message;
        }
      });
    }
  }
}
