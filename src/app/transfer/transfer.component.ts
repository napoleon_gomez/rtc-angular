import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";

import { BalanceService }  from "../balance/balance.service";

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {
  //appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  loading:  boolean = false;
  message:  string  = '';
  status:   boolean = false;

  transferForm:     FormGroup;
  transferRut:      FormControl;
  transferAmount:   FormControl;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private balanceService: BalanceService) {
    this.transferForm = formBuilder.group({
      transferRut:    [this.transferRut, [Validators.required, Validators.minLength(9), Validators.pattern('[0-9]{7,8}-[0-9Kk]{1}')]],
      transferAmount: [this.transferAmount, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit(): void {
  }

  transferAmountFromMyAccount(value: any = {}) {
    if(this.transferForm.valid) {
      let form = {
        id: localStorage.getItem('userId'),
        rut: value.transferRut,
        amount: value.transferAmount
      }
      this.loading = true;
      this.httpClient.post<any[]>(this.appBaseUrl + '/transfer', form).subscribe((res: any) => {
        this.loading = false;
        this.status = res.status;
        this.message = res.message;
        if(res.status) {
          localStorage.setItem('userBalance', res.data.balance);
          this.balanceService.balanceChange(res.data.balance);
          this.transferForm.get('transferRut')!.setValue('');
          this.transferForm.get('transferAmount')!.setValue(0);
        }
        setTimeout(() => {
          this.message = '';
          this.status = false;
        }, 5000, this);
      });
    }
  }
}
