import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { BalanceService }  from "../balance/balance.service";

@Component({
  selector: 'app-user-transactions',
  templateUrl: './user-transactions.component.html',
  styleUrls: ['./user-transactions.component.css']
})
export class UserTransactionsComponent implements OnInit {
  //appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  loading:  boolean = false;
  message:  string  = '';
  status:   boolean = false;

  transactions = [];
  balance: number = 0;

  constructor(private httpClient: HttpClient, private balanceService: BalanceService) { }

  ngOnInit(): void {
    window.history.pushState(null, '/transactions', document.URL);
    window.addEventListener('popstate', function () {
      window.history.pushState(null, '/transactions', document.URL);
    });

    let id = { id: localStorage.getItem('userId') };

    this.loading = true;
    this.httpClient.post<any[]>(this.appBaseUrl + '/transactions', id).subscribe((res: any) => {
      this.loading = false;
      this.status = res.status;
      this.message = res.message;

      if(res.status && res.data.transactions.length > 0) {
        this.transactions = res.data.transactions.reverse();
        this.balance = parseFloat(res.data.balance);
      }
    });
  }

}
