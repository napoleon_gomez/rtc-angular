import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";

import { BalanceService }  from "../balance/balance.service";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  //appBaseUrl = 'http://localhost:8082/api';
  appBaseUrl = 'https://rtc-backend-chi.vercel.app/api';

  loading:  boolean = false;
  message:  string = '';
  status:   boolean = false;

  withdrawForm:     FormGroup;
  withdrawAmount:   FormControl;

  constructor(private httpClient: HttpClient, private formBuilder: FormBuilder, private balanceService: BalanceService) {
    this.withdrawForm = formBuilder.group({
      withdrawAmount: [this.withdrawAmount, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit(): void {
  }

  withdrawAmountFromMyAccount(value: any = {}) {
    if(this.withdrawForm.valid) {
      let form = {
        id: localStorage.getItem('userId'),
        amount: value.withdrawAmount
      }
      this.loading = true;
      this.httpClient.post<any[]>(this.appBaseUrl + '/withdraw', form).subscribe((res: any) => {
        this.loading = false;
        this.status = res.status;
        this.message = res.message;
        if(res.status) {
          localStorage.setItem('userBalance', res.data.balance);
          this.balanceService.balanceChange(res.data.balance);
          this.withdrawForm.get('withdrawAmount')!.setValue(0);
        }
        setTimeout(() => {
          this.message = '';
          this.status = false;
        }, 5000, this);
      });
    }
  }
}
